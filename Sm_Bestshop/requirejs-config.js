var config = {
	map: {
		'*': {
			jquerybootstrap: "Sm_Bestshop/js/bootstrap/bootstrap.min",
			owlcarousel: "Sm_Bestshop/js/owl.carousel",
			jqueryfancyboxpack: "Sm_Bestshop/js/jquery.fancybox.pack",
			jqueryunveil: "Sm_Bestshop/js/jquery.unveil",
			yttheme: "Sm_Bestshop/js/yttheme"
		}
	}
};